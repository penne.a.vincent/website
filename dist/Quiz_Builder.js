document.title = "Penne's Quiz Builder"

// () these contain the 'argument/s' but can be empty - but must be there for function syntax
function TestFunctionArguments(){
  DoStuff (3,5)
  DoStuff (36,5)
  DoStuff (3,65)
}
function DoStuff(x,y){
  console.log (x+y)
}

class Quiz {
  //Pale Blue = property or data
  // Yellow = Function (in the context of a class), purple = decision making (control flow)
  //dark blue = other key words - special variable such as function, let
  // [] = empty array
  QuestionLabels = []
  // array that starts with values - in this case used each time
  ResponseTitles = ["Poor","Not Good","OK","Great","Excellent"]
  // now we define the render function 
  // Parent is the 'argument' - whatever we parse in becomes the parent 
  Render (Parent) {
    let RenderedChildren = []
    for (let j = 0; j <this.QuestionLabels.length; j++) {
      let QuestionNumber = j + 1
      // full stop accesses properties or functions of an object
      // creates a p tag and save a reference to it that we are calling QuestionLabel
      let QuestionLabel = document.createElement("p")
      //creates a div tag and calls it QuestionContainer
      let QuestionContainer = document.createElement("div")
      //adding the j-th question that has been input
      QuestionLabel.textContent = this.QuestionLabels[j]
      //adding the QuestionLabel as a child in the container
      QuestionContainer.appendChild (QuestionLabel)
      for (let i = 0; i <this.ResponseTitles.length; i++) {
          let RadioButton = document.createElement("input")
          RadioButton.type = "radio"
          RadioButton.value = `${i+1}`
          RadioButton.name = `Question_${QuestionNumber}`
          RadioButton.id = `Q${QuestionNumber}_${i+1}`
          //addung the next radio button in the container - the button - not the ResponseTitles text
          QuestionContainer.appendChild (RadioButton)
          //label similar to p but label and input go together in radio buttons
          let RadioLabel = document.createElement("label")
          RadioLabel.htmlFor = `Q${QuestionNumber}_${i+1}`
          RadioLabel.textContent = this.ResponseTitles[i]
          //adds 1 ResponseTitle text
          QuestionContainer.appendChild (RadioLabel)
          //adding the break to the container so each new response is on a new line
          QuestionContainer.appendChild (document.createElement("br"))
      }
      //push adds to the end of the array - so pushed j times - number of questions
      RenderedChildren.push (QuestionContainer)
      console.log("Adding new question")
      QuestionNumber ++
    }
    //parent.replaceChildren() means delete all children of this element
  Parent.replaceChildren()
  //now - adds all the questions to the page, and does this each time we add a new question and press the button
  for (let k = 0; k <RenderedChildren.length;k++){
    Parent.appendChild(RenderedChildren[k])
  }
  }
}
//This sits here so we create it just once, not each time we add a new questions
var QuizData = new Quiz ()
function MakeQuestion (
  )
  {
    //Get the new question from the page by typing it in
    let NextQuestionText = document.getElementById("QuizBuilderNextQuestionText")
    console.log("Creating question with text",NextQuestionText.value)
    //Tells the quiz where to locate the div tag that will contain all questions
    let Container = document.getElementById("QuizRenderArea")
    //Adding the question from input text box to the list of current questions
    QuizData.QuestionLabels.push(NextQuestionText.value)
    //When function has argument - (xxx) - it is a way to take in data
    //now putting the container - where all the questions will go - into the function
    QuizData.Render (Container)
    console.log("Adding new question")
 


    
  }