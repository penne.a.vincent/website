let paragraphTags = document.getElementsByTagName("p");

document.title = "Penne's Webpage"
for (let i = 0; i <paragraphTags.length; i++) {
    console.log(i)
    paragraphTags[i].style.color = colorcode();
}

// A function that makes colour codes 
function colorcode () {
    let code="#"
    let r = Math.random()*256
    let g = Math.random()*256
    let b = Math.random()*256
    r = Math.floor(r)
    g = Math.floor(g)
    b = Math.floor(b)
    console.log(r,g,b)
    let rstring = r.toString(16)
    let gstring = g.toString(16)
    let bstring = b.toString(16)
    console.log(rstring, gstring, bstring)

    if(rstring.length < 2){
        rstring="0"+rstring
    }
    if(gstring.length < 2){
        gstring="0"+gstring
    }
    if(bstring.length < 2){
        bstring="0"+bstring
    }
    console.log(rstring, gstring, bstring)
    code = code +  rstring + gstring + bstring
    return code

} 

// A function to create a button to take us to another page
function gotoQuizBuilder (event) {
    console.log("Going to Quiz Builder")
    window.location.href = "./Quiz_Builder.html"
 }